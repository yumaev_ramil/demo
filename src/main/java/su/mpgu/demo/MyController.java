package su.mpgu.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Yumaev Ramil
 * created: 2023-05-30 14:02
 */
@Controller
public class MyController {

    @GetMapping("/")
    public String homePages() {
        return "index";
    }

}
