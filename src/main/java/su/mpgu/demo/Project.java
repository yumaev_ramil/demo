package su.mpgu.demo;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
/**
 * @author Yumaev Ramil
 * created: 2023-05-30 15:43
 */
@Data
public class Project {

    private int id;

    @NotBlank(message = "Поле не должно быть пустым!")
    private String name;

}
