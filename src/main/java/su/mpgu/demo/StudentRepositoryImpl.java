package su.mpgu.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;

/**
 * @author Yumaev Ramil
 * created: 2023-06-04 12:19
 */

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    @Autowired
    private JdbcOperations jdbcOperations;

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public void save(Student student) {
        jdbcOperations.update("INSERT INTO STUDENTS "
                        + "(LAST_NAME, FIRST_NAME, PATRONYMIC, PROJECT_ID) VALUES(?,?,?,?)",
                student.getLastName(),
                student.getFirstName(),
                student.getPatronymic(),
                student.getProjectId());
    }

    @Override
    public void update(Student student) {
        jdbcOperations.update(
                "UPDATE STUDENTS " +
                        "SET LAST_NAME = ?, "
                        + "FIRST_NAME = ?, "
                        + "PATRONYMIC = ?, "
                        + "PROJECT_ID = ? "
                        + "WHERE ID = ?",
                student.getLastName(),
                student.getFirstName(),
                student.getPatronymic(),
                student.getProjectId(),
                student.getId()
        );
    }

    @Override
    public void delete(int id) {
        jdbcOperations.update("DELETE FROM STUDENTS WHERE ID = ?", id);
    }

    @Override
    public List<Student> findAll() {
        return jdbcOperations.query("SELECT * FROM STUDENTS", this::mapRowToStudent);
    }

    @Override
    public Student findById(int id) {
        return jdbcOperations.queryForObject("SELECT * FROM STUDENTS WHERE ID = " + id, this::mapRowToStudent);
    }

    private Student mapRowToStudent(ResultSet row, int rowNum) throws SQLException {
        Student student = new Student();
        student.setId(row.getInt("ID"));
        student.setLastName(row.getString("LAST_NAME"));
        student.setFirstName(row.getString("FIRST_NAME"));
        student.setPatronymic(row.getString("PATRONYMIC"));
        student.setProjectId(row.getInt("PROJECT_ID"));
        return student;
    }

}
