package su.mpgu.demo;

import java.util.List;

/**
 * @author Yumaev Ramil
 * created: 2023-05-31 21:24
 */
public interface ProjectRepository {

    void save(final Project project);

    List<Project> findAll();

    Project findById(int id);

    void delete(int id);

    void update(final Project project);

}
