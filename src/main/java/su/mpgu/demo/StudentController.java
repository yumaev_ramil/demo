package su.mpgu.demo;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author Yumaev Ramil
 * created: 2023-06-04 13:14
 */

@Controller
@RequestMapping("/students")
public class StudentController {

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("students/index");
        modelAndView.addObject("students", studentRepository.findAll());
        return modelAndView;
    }

    @GetMapping("create")
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("students/insert");
        Student student = new Student();
        List<Project> projects = projectRepository.findAll();
        modelAndView.addObject("projects", projects);
        modelAndView.addObject("student", student);
        modelAndView.addObject("title", "Добавление");
        return modelAndView;
    }

    @PostMapping("save")
    public ModelAndView save(@Valid Student student, Errors errors) {
        if (errors.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("students/insert");
            List<Project> projects = projectRepository.findAll();
            modelAndView.addObject("projects", projects);
            modelAndView.addObject("student", student);
            modelAndView.addObject("title", "Ошибка валидации");
            return modelAndView;
        }
        studentRepository.save(student);
        return new ModelAndView("redirect:/students/index");
    }

    @PostMapping("edit")
    public ModelAndView edit(@Valid Student student, Errors errors) {
        if (errors.hasErrors()) {
            this.update(student.getId());
        }
        studentRepository.update(student);
        return new ModelAndView("redirect:/students/index");
    }

    @GetMapping("update")
    public ModelAndView update(@RequestParam int id) {
        ModelAndView modelAndView = new ModelAndView("students/edit");
        Student student = studentRepository.findById(id);
        modelAndView.addObject("student", student);
        List<Project> projects = projectRepository.findAll();
        modelAndView.addObject("projects", projects);
        modelAndView.addObject("title", "Редактирование");
        return modelAndView;
    }

    @GetMapping("delete")
    public String delete(@RequestParam int id) {
        studentRepository.delete(id);
        return "redirect:/students/index";
    }

}
