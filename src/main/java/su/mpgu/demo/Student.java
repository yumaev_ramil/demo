package su.mpgu.demo;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author Yumaev Ramil
 * created: 2023-05-30 14:53
 */

@Data
public class Student {

    private int id;

    @NotBlank(message = "Поле не должно быть пустым!")
    private String lastName;

    @NotBlank(message = "Поле не должно быть пустым!")
    private String firstName;

    @NotBlank(message = "Поле не должно быть пустым!")
    private String patronymic;

    @NotNull(message = "Надо выбрать проект!")
    private Integer projectId;

}
