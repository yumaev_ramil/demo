package su.mpgu.demo;

import java.util.List;

/**
 * @author Yumaev Ramil
 * created: 2023-06-04 12:10
 */
public interface StudentRepository {

    void save(Student student);

    void update(Student student);

    void delete(int id);

    List<Student> findAll();

    Student findById(int id);

}
