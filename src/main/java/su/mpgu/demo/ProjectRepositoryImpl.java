package su.mpgu.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Yumaev Ramil
 * created: 2023-05-31 21:26
 */

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {

    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public void save(Project project) {
        jdbcOperations.update("INSERT INTO PROJECTS (NAME) VALUES(?)", project.getName());
    }

    @Override
    public List<Project> findAll() {
        return jdbcOperations.query("SELECT * FROM PROJECTS ", this::mapRowToProject);
    }

    @Override
    public Project findById(int id) {
        return jdbcOperations.queryForObject("SELECT * FROM PROJECTS WHERE ID =" + id, this::mapRowToProject);
    }

    @Override
    public void delete(int id) {
        jdbcOperations.update("DELETE FROM PROJECTS WHERE ID = ?", id);
    }

    @Override
    public void update(Project project) {
        jdbcOperations.update("UPDATE PROJECTS SET NAME = ? WHERE ID = ?", project.getName(), project.getId());
    }

    private Project mapRowToProject(ResultSet row, int rowNum) throws SQLException {
        Project project = new Project();
        project.setId(row.getInt("id"));
        project.setName(row.getString("name"));
        return project;
    }

}
