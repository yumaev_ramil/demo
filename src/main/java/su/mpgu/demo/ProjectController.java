package su.mpgu.demo;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Yumaev Ramil
 * created: 2023-05-30 17:36
 */

@Controller
@RequestMapping("/projects/")
public class ProjectController {

    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("projects/index");
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

    @GetMapping("create")
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("projects/insert");
        Project project = new Project();
        modelAndView.addObject("project", project);
        modelAndView.addObject("title", "Создание");
        return modelAndView;
    }

    @PostMapping("edit")
    public ModelAndView edit(@Valid Project project, Errors errors) {
        if (errors.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("projects/edit");
            modelAndView.addObject("project", project);
            modelAndView.addObject("title", "Ошибка валидации");
            return modelAndView;
        }
        projectRepository.update(project);
        return new ModelAndView("redirect:/projects/index");
    }

    @PostMapping("save")
    public ModelAndView save(@Valid Project project, Errors errors) {
        if (errors.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("projects/form");
            modelAndView.addObject("project", project);
            modelAndView.addObject("title", "Ошибка валидации");
            return modelAndView;
        }
        projectRepository.save(project);
        return new ModelAndView("redirect:/projects/index");
    }

    @GetMapping("delete")
    public ModelAndView delete(@RequestParam int id) {
        projectRepository.delete(id);
        return new ModelAndView("redirect:/projects/index");
    }

    @GetMapping("update")
    public ModelAndView update(@RequestParam int id) {
        ModelAndView modelAndView = new ModelAndView("projects/edit");
        modelAndView.addObject("project", projectRepository.findById(id));
        modelAndView.addObject("title", "Редактирование");
        return modelAndView;
    }

}
